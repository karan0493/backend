const galleryModel = require('../models/Gallery.models');


const addGallery = (req, res) => {
    const file = req.body;

    if (!file) {
        res.status(400).send("Please select a file")
    }else {
        const fileDetails = new galleryModel(file);

        fileDetails.save().then(() => {
            res.status(201).send({
                status: "success",
                error: 'false',
                data: fileDetails
            });
        }).catch((e) => {
            res.status(400).send(e)
        }, []);
    };
};

const allImage = async (req, res) => {
    try {
        const data = await galleryModel.find();
        if (!data) {
            res.status(400).send("internal error");
        } else {
            res.status(201).send({
                status: "success",
                error: 'false',
                data: data
            });
        };
    } catch (err) {
        console.log(err);
        res.status(400).send(e)
    };
};

const deleteImage = async (req, res) => {
    const id = req.params.id
    try {
        const data = await galleryModel.findByIdAndDelete(id);
        if (!data) {
            res.status(400).send("internal error");
        } else {
            res.status(201).send({
                status: "success",
                error: 'false',
                data: data
            });
        };
    } catch (err) {
        console.log(err);
        res.status(400).send(e)
    };
};

module.exports = { addGallery, allImage, deleteImage }