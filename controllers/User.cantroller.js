const User = require('../models/User.models');
const bycrpt = require('bcrypt');
const jwt = require("jsonwebtoken");
const nodemailer = require("nodemailer");
const config = require("../Config");
const rendomstring = require('randomstring')

// const sendMails = async function (name, email, password) {
//     try {
//         const transpoter = nodemailer.createTransport({
//             host: "smtp.email.com",
//             port: 587,
//             secure: false,
//             auth: {
//                 user: config.AdminEmail,
//                 pass: config.AdminPass
//             }
//         });

//         const mail = {
//             from: config.AdminEmail,
//             to: email,
//             subject: "For password Reset",
//             html: 'hi ' + password + ' there '
//         };

//         transpoter.sendMail(mail, function (error, info) {
//             if (error) {
//                 console.log(error);
//             } else {
//                 console.log("mail sent");
//             };
//         });
//     } catch (err) {
//         console.log(err);
//     };
// };

const createUser = (async (req, res) => {
    const data = req.body;

    try {
        const userExist = await User.findOne({ email: data.email });
        if (userExist) {
            return res.status(422).send({ err: "User Allrady exist" });
        } else {
            const user = new User(data);
            const save = await user.save()
            if (save) {
                res.status(201).send({
                    status: "success",
                    error: 'false',
                    data: data
                });
            } else {
                res.status(500).json({ err: "Internal error" })
            };
        }
    }
    catch (error) {
        console.log(error);
    };
});

const signIn = (async (req, res) => {
    const { email, password } = req.body;

    try {
        const userExist = await User.findOne({ email });
        if (userExist) {
            const passMatch = await bycrpt.compare(password, userExist.password);
            if (!passMatch) {
                res.status(400).send({ err: "invalid Crediatial" });
            } else {
                const token = await userExist.generateToken();
                res.cookie("jwttoken", token, {
                    maxAge: '900000',
                    httpOnly: true
                });
                if (!token) {

                }
                res.status(201).send({ massge: "saccessfully login" });
            };
        } else {
            return res.status(422).send({ err: "Unarthorazed User" });
        };
    }
    catch (error) {
        console.log(error);
    };
});


const fargotPass = (async (req, res) => {
    const  {email}  = req.body;
    console.log(email);

    try {
        const userExist = await User.findOne({ email });
        if (!userExist) {
            res.status(422).send({ msg: "User Not Found!" });
        } else {
            const otp = rendomstring.generate({
                length: 4,
                charset: 'numeric'
            });
            console.log(userExist);

            const transporter = nodemailer.createTransport({
                service: 'gmail',
                auth: {
                    user: "gkv8878@gmail.com",
                    pass: config.EMAIL_PASSWORD,
                },
            });

            const mailOptions = {
                from: config.EMAIL_USERNAME,
                to: email,
                subject: 'Password Reset',
                text: `Your OTP for password reset is: ${otp}`,
            };

            transporter.sendMail(mailOptions, (error, info) => {
                if (error) {
                    console.log(error);
                    res.status(500).json({ message: 'Failed to send email' });
                } else {
                    res.status(201).json({
                        status:"success", 
                        message: 'Email sent successfully',
                        data:email
                     });

                    User.findOneAndUpdate({ email: email }, { otp }, { new: true })
                    .then((user) => {
                      console.log('OTP saved:', user.otp);
                    })
                    .catch((error) => {
                      console.error('Error saving OTP:', error);
                    });
                };
            });
        };
    }
    catch (error) {
        console.log(error);
    };
});

const changePass = (async(req,res)=>{
    const {otp,email,password} = req.body;
    try {
        const varifyOtp = User.findOne({email});
        console.log(email);
        if (!varifyOtp) {
            res.status(422).send({ msg: "User Not Found!" });
        } else {

        }
    } catch (err) {
        console.log(err);
    };
});

const allUser = (async (req, res) => {
    try {
        const data = await User.find();
        if (!data) {
            res.status(400).send("internal error");
        } else {
            res.status(201).send({
                status: "success",
                error: 'false',
                data: data
            });
        };
    } catch (err) {
        console.log(err);
        res.status(400).send(e)
    };
});

const getUser = (async (req, res) => {
    const id = req.params.id;
    try {
        const data = await User.findById(id);
        if (!data) {
            res.status(400).send("internal error");
        } else {
            res.status(201).send({
                status: "success",
                error: 'false',
                data: data
            });
        };
    } catch (err) {
        console.log(err);
        res.status(400).send(e)
    };
});


const deleteUser = (async (req, res) => {
    const id = req.params.id;
    try {
        const data = await User.findByIdAndDelete(id);
        if (!data) {
            res.status(400).send("internal error");
        } else {
            res.status(201).send({
                status: "success",
                error: 'false',
                data: data
            });
        };
    } catch (err) {
        console.log(err);
        res.status(400).send(e)
    };
});


module.exports = { createUser, signIn, fargotPass, allUser, getUser, deleteUser,changePass };