const TecnologyModels = require("../models/Tecnology.models");



const addTecnology = ( async function(req,res){
    const data = req.body;
    try {
      
            const tech = new TecnologyModels(data);
            const save = await tech.save()
            if (save) {
                res.status(201).send({
                    status: "success",
                    error: 'false',
                    data: data
                });
            } else {
                res.status(500).json({ err: "Internal error" })
            };
        }
    catch (error) {
        console.log(error);
    };
});

const allTecnology = ( async function(req,res){
    const data = req.body;
    try {
        const data = await TecnologyModels.find();
        if (!data) {
            res.status(400).send("internal error");
        } else {
            res.status(201).send({
                status: "success",
                error: 'false',
                data: data
            });
        };
    } catch (err) {
        console.log(err);
        res.status(400).send(e)
    };
});

const updateTecnology = ( async function(req,res){
    const datas = req.body;
    const id = req.params.id;

    try {
        const data = await TecnologyModels.findByIdAndUpdate(id,datas, { new: true }); 
        if (!data) {
            res.status(400).send("internal error");
        } else {
            res.status(201).send({
                status: "success",
                error: 'false',
                data: data
            });
        };
    } catch (err) {
        console.log(err);
        res.status(400).send(e)
    };
});

const deleteTecnology = ( async function(req,res){
    const id = req.params.id;

    try {
        const data = await TecnologyModels.findByIdAndDelete(id); 
        if (!data) {
            res.status(400).send("internal error");
        } else {
            res.status(201).send({
                status: "success",
                error: 'false',
                data: data
            });
        };
    } catch (err) {
        console.log(err);
        res.status(400).send(e)
    };
});

module.exports={addTecnology,allTecnology,updateTecnology,deleteTecnology}