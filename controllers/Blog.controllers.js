const blogSchema = require("../models/Blog.models");
const moment = require('moment');

const addBlog = (async (req, res) => {
    const { title, publiser, aurther, description, image, category, content, types, contentimg } = req.body
    try {
        if (!title || !publiser || !aurther || !description || !image || !category || !content || !types || !contentimg) {
            res.status(404).send("Please Fill All Data")
        } else {

            const blog = new blogSchema({ title, publiser, aurther, description, image, category, types, date: moment().format("MMM DD YY"), content, contentimg });
            const save = await blog.save();
            if (save) {
                res.status(201).send({
                    status: "success",
                    error: 'false',
                    data: blog
                });
            } else {
                res.status(500).send("Internal error");
            };
        };
    } catch (err) {
        console.log(err);
    };
});


const allBlog = (async (req, res) => {
    try {
        const { category } = req.query;
        const queryObject = {};
        if (category) {
            queryObject.category = category
        }
        const pageSize = req.query.limit || 6; // Number of documents per page
        const currentPage = req.query.page || 1; // Current page number

        const skip = (currentPage - 1) * pageSize;

        const count = await blogSchema.count(queryObject);
        const totalPages = Math.ceil(count / pageSize)


        const data = await blogSchema.find(queryObject).skip(skip).limit(pageSize);
        if (!data) {
            res.status(400).send("internal error");
        } else {
           
            res.status(201).send({
                status: "success",
                error: 'false',
                data: data,
                totalPages: totalPages
            });
        };

    } catch (err) {
        console.log(err);

    };
});

const singleBlog = (async (req, res) => {
    const { id } = req.params
    try {
        const data = await blogSchema.findById(id);
        if (!data) {
            res.status(400).send("internal error");
        } else {
            res.status(201).send({
                status: "success",
                error: 'false',
                data: data,
            });
        };
    } catch (err) {
        console.log(err);
        res.send(err);
    };
});



const deleteBlog = (async (req, res) => {
    const id = req.params.id
    try {
        const data = await blogSchema.findByIdAndDelete(id);
        if (!data) {
            res.status(400).send("internal error");
        } else {
            res.status(201).send({
                status: "success",
                error: 'false',
                data: data
            });
        };
    } catch (err) {
        console.log(err);
    };
});

const updateBlog = (async function (req, res) {
    const upadate = req.body;
    const id = req.params.id;

    try {
        const data = await blogSchema.findByIdAndUpdate(id, upadate, { new: true });
        if (!data) {
            res.status(500).send("internal error");
        } else {
            res.status(201).send({
                status: "success",
                error: 'false',
                data: data
            });
        };
    } catch (err) {
        console.log(err);
        res.status(400).send(e)
    };

});
const hello =( async function (req,res){
    res.send("hello from backend");
});

module.exports = { addBlog, allBlog, deleteBlog, updateBlog, singleBlog,hello };