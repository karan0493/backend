const inqurySchema = require("../models/Inqury.models");

const postInqury = async (req, res) => {
    const { name, email, subject, message } = req.body;
    try {
        if (!name || !email || !subject || !message) throw new Error("data is not found");
        const data = await inqurySchema({ name, email, subject, message });
        const save = await data.save();

        if (!save) throw new Error("internal server error")

        res.send({
            status: "success",
            error: "false",
            data: data
        });
    } catch (err) {
        console.log(err);
        res.send(err)
    };
};

const getInqury = async (req, res) => {
   
    try {  
        const pageSize = req.query.limit || 4; // Number of documents per page
        const currentPage = req.query.page || 1; // Current page number
        const skip = (currentPage - 1) * pageSize;
        
        const count = await inqurySchema.count();
        const totalPages = Math.ceil(count / pageSize)

        const data = await inqurySchema.find().skip(skip).limit(pageSize);
        if (!data){
            res.send('intarnal error');
        }else{
        res.send({
            status: "success",
            error: "false",
            data: data,
            totalPages:totalPages
        });
    }
    } catch (err) {
        console.log(err);
        res.send(err);
    };
};

const singalInqury = async (req, res) => {
   const {id} =req.params;
    try {  
        const data = await inqurySchema.findById(id);
        if (!data){
            res.send('intarnal error');
        }else{
        res.send({
            status: "success",
            error: "false",
            data: data,
        });
    }
    } catch (err) {
        console.log(err);
        res.send(err);
    };
};

const deleteInqury = (async (req, res) => { 
    const id =req.params.id
    try {
        const data = await inqurySchema.findByIdAndDelete(id);
        if (!data) {
            res.status(400).send("internal error");
        } else {
            res.status(201).send({
                status: "success",
                error: 'false',
                data: data
            });
        };
    } catch (err) {
        console.log(err);
    };
});

module.exports = { postInqury,getInqury,deleteInqury,singalInqury }