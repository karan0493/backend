const {addGallery,allImage,deleteImage} = require('../controllers/Gallery.controllers');
const express = require('express');
const routes = express.Router();
const upload = require("../middlware/multer");


routes.get('/gallery',allImage);
routes.post('/gallery',addGallery);
routes.delete('/gallery/:id',deleteImage);   
  
module.exports =routes