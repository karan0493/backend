const express = require('express');
const routes = express.Router();
const {addBlog,allBlog,deleteBlog,updateBlog,singleBlog,hello} =require("../controllers/Blog.controllers")

routes.post("/blog",addBlog);
routes.get("/blog",allBlog);
routes.get('/blog/:id',singleBlog);
routes.delete("/blog/:id",deleteBlog);
routes.patch("/blog/:id",updateBlog);
routes.get("/hello",hello);

module.exports=routes;