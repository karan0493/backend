const {postInqury,getInqury,deleteInqury,singalInqury} = require('../controllers/Inqury.controllers');
const express =require('express');
const routes =express.Router();

routes.post("/inqury",postInqury);
routes.get("/inqury",getInqury);
routes.get("/inqury/:id",singalInqury);
routes.delete("/inqury/:id",deleteInqury);

module.exports=routes; 