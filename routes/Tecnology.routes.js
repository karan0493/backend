const {addTecnology,allTecnology,updateTecnology,deleteTecnology} =require("../controllers/Tecnology.controllers");
const express = require("express");
const router = express.Router();

router.post("/tecnology",addTecnology);
router.get("/tecnology",allTecnology);
router.patch("/tecnology/:id",updateTecnology);
router.delete("/tecnology/:id",deleteTecnology);

module.exports=router;