const express = require('express');
const routes = express.Router();

const {createUser,allUser,getUser,deleteUser,fargotPass,changePass} = require("../controllers/User.cantroller");

routes.post("/user",createUser);
routes.get("/user",allUser);
routes.get("/user/:id",getUser);
routes.delete("/user/:id",deleteUser);

routes.post('/fargotpass',fargotPass);
routes.post('/changpass',changePass);

module.exports=routes;