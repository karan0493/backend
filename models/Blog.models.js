const mongoose = require("mongoose");

const BlogSchema = new mongoose.Schema({
    title: {
        type: String,
        require: true
    },
    publiser: {
        type: String,
        require: true
    },
    aurther: {
        type: String,
        require: true
    },
    image: {
        type: String,
        require: true
    },
    description: {
        type: String,
        require: true
    },
    category: {
        type: String,
        require: true
    },
    types: {
        type: String,
        require: true
    },
    date: {
        type: String
    },
    content: {
        type: Object
    },
    contentimg:{
        type: String,
    }


});

module.exports = mongoose.model("blog", BlogSchema);