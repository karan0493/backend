const mongoose = require('mongoose');
const { create } = require('./Blog.models');

const InqurySchema = new mongoose.Schema({
    name:{
        type:String,
        require:true
    },
    email:{
        type:String,
        require:true 
    },
    subject:{
        type:String,
        require:true
    },
    message:{
        type:String,
        require:true
    }
});

module.exports = mongoose.model("inqury",InqurySchema);