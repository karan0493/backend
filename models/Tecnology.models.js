const mongoose = require("mongoose");

const TecnologySchema = mongoose.Schema({
    name:{
        type:String,
        require:true
    },
    icon:{
        type:String,
        require:true
    },
    description:{
        type:String,
        require:true
    }
})
module.exports= mongoose.model("Tecnology",TecnologySchema);