const mongoose = require('mongoose');

const GallerySchema = new mongoose.Schema({
   path:{
    type:String,
    require:true
   }
});

module.exports=mongoose.model("Gallery",GallerySchema);