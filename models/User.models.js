const mongoose = require('mongoose');
const bycrpt = require('bcrypt');
const jwt = require('jsonwebtoken');

const UserSchema = new mongoose.Schema({
    name: {
        type: String,
        require: true
    },
    email: {
        type: String,
        require: true
    },
    mobile: {
        type: Number,
        require: true
    },
    password: {
        type: String,
        require: true
    },
    gender: {
        type: String,
        require: true
    },
    tokens: {
        type: String,
        require: true
    },
    otp: {
        type: Number
    }

});

UserSchema.pre('save', async function (next) {
    if (this.isModified("password")) {
        this.password = await bycrpt.hash(this.password, 12);
    };
    next();
});

UserSchema.methods.generateToken = async function () {
    try {
        const token = jwt.sign({ _id: this._id }, process.env.SECRET_KEY);
        this.tokens = token
        this.save();
        return token;
    } catch (err) {
        console.log(err);
    };
};

module.exports = mongoose.model("User", UserSchema);