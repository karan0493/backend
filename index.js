const express = require("express");
require("dotenv").config();
const port = process.env.PORT || 8080;
const app = express();
const cors = require("cors");
const bodyParser = require('body-parser');
const coockeParse = require("cookie-parser");


const user = require("./routes/User.routes");
const signin = require("./routes/Login.routes");
const gallery = require('./routes/Gallery.routes');
const tecnology = require('./routes/Tecnology.routes');
const blog =require("./routes/Blog.routes");
const inqury =require('./routes/Inqury.routes');



// const getBaseUrl = ()=>{
// var HostName = window.location.hostname
// var ReturnURL = '';

// if (HostName.indexOf("staging.cc") > 0) {  //Live
//     ReturnURL = 'https://technolee.api.thestaging.cc'
// }else {  //Local
//      ReturnURL = ''
//  }

//  return ReturnURL
// }

require("./database/db");

app.use(bodyParser.json({limit: '50mb'}));
app.use(bodyParser.urlencoded({limit: '50mb', extended: true}));
app.use(express.json());
app.use(cors({origin:["https://technolee.api.thestaging.cc", "http://localhost:5000","https://technolee.hello.thestaging.cc","https://technolee.thestaging.cc"]}))
app.use(coockeParse());
app.use(express.static(__dirname));


app.use(user);
app.use(signin);
app.use(gallery);
app.use(tecnology);
app.use(blog); 
app.use(inqury);

app.listen(port, () => {
     console.log(`Server is running at port ${port}`);
}); 